package edu.uchicago.skoop.jetpackproject2;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;

import java.util.List;

// https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#14

public class PlaceRepository {

    private PlaceDao placeDao;
    private LiveData<List<Place>> allPlaces;

    PlaceRepository(Application application) {
        PlaceDatabase DB = PlaceDatabase.getDatabase(application);
        placeDao = DB.placeDao();
        allPlaces = placeDao.getAllPlaceData();
    }

    LiveData<List<Place>> getAllPlaces() {
        return allPlaces;
    }

    public void insertPlace (Place place) {
        new InsertAsyncTask(placeDao).execute(place);
    }

    private static class InsertAsyncTask extends AsyncTask<Place, Void, Void> {

        private PlaceDao asyncTaskDao;

        InsertAsyncTask(PlaceDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Place... params) {
            asyncTaskDao.insertPlace(params[0]);
            return null;
        }
    }
}
