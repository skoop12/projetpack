package edu.uchicago.skoop.jetpackproject2;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

// https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#14

@Dao
public interface PlaceDao {

    @Insert
    void insertPlace(Place place);

    @Query("SELECT * from places_table ORDER BY locationName ASC")
    LiveData<List<Place>> getAllPlaceData();

    @Query("SELECT * from places_table LIMIT 1")
    Place[] checkForEmpty();
}
