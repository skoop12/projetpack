package edu.uchicago.skoop.jetpackproject2;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#14

@Entity(tableName = "places_table")
public class Place {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "locationName")
    private String locationName;

    @NonNull
    @ColumnInfo(name = "dateVisited")
    private String dateVisited;

    @NonNull
    @ColumnInfo(name = "latitude")
    private String latitude;

    @NonNull
    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "emoji")
    private String emoji;

    public Place(int id, String locationName, String dateVisited, String latitude, String longitude, String emoji) {
        this.id = id;
        this.locationName = locationName;
        this.dateVisited = dateVisited;
        this.latitude = latitude;
        this.longitude = longitude;
        this.emoji = emoji;
    }

    public int getId() {
        return this.id;
    }

    public String getLocationName() {
        return this.locationName;
    }

    public String getDateVisited() {
        return this.dateVisited;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public String getEmoji(){ return this.emoji;  }
}
