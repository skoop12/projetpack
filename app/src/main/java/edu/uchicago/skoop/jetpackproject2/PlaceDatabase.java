package edu.uchicago.skoop.jetpackproject2;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

// https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#14

@Database(entities = {Place.class}, version = 2)
public abstract class PlaceDatabase extends RoomDatabase {

    public abstract PlaceDao placeDao();

    private static volatile PlaceDatabase INSTANCE;

    static PlaceDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PlaceDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), PlaceDatabase.class, "places_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(placeDatabaseCallback).build();
                }
            }
        }
        return INSTANCE;
    }

    private static PlaceDatabase.Callback placeDatabaseCallback = new PlaceDatabase.Callback(){

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase DB) {
            super.onOpen(DB);
            new PopulateDBAsync(INSTANCE).execute();
        }
    };


    private static class PopulateDBAsync extends AsyncTask<Void, Void, Void> {

        private final PlaceDao placeDao;

        PopulateDBAsync(PlaceDatabase DB) {
            placeDao = DB.placeDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            if (placeDao.checkForEmpty().length < 1) {
                Place place = new Place(1, "Park", "5/17/19", "41.898639", "-87.626766", new String(Character.toChars(0x2764)));
                placeDao.insertPlace(place);
                place = new Place(2, "Coffee Shop", "5/13/19", "41.940102", "-87.6582762", new String(Character.toChars(	0x2615)));
                placeDao.insertPlace(place);
            }
            return null;
        }
    }
}
