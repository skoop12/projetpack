package edu.uchicago.skoop.jetpackproject2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.emoji.widget.EmojiAppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

// https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#14

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> {

    class PlaceViewHolder extends RecyclerView.ViewHolder {

        private final TextView locationText;
        private final TextView dateText;
        private final LinearLayout rowLayout;
        private EmojiAppCompatTextView emojiAppCompatTextView;
        private String latitude;
        private String longitude;

        private PlaceViewHolder(View itemView) {
            super(itemView);
            emojiAppCompatTextView = itemView.findViewById(R.id.emoji);
            locationText = itemView.findViewById(R.id.locationText);
            dateText = itemView.findViewById(R.id.dateText);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }

    private final LayoutInflater layoutInflater;
    private List<Place> places;
    private Context context;

    PlaceAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.places_row, parent, false);
        return new PlaceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PlaceViewHolder holder, int position) {
        if (places != null) {
            Place current = places.get(position);
            holder.locationText.setText(current.getLocationName());
            holder.dateText.setText(current.getDateVisited());
            holder.latitude = current.getLatitude();
            holder.emojiAppCompatTextView.setText(current.getEmoji());
            holder.longitude = current.getLongitude();
        } else {
            holder.locationText.setText("");
            holder.dateText.setText("");
        }
        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putExtrasOnIntent(holder);
            }
        });
    }

    void putExtrasOnIntent(PlaceViewHolder holder) {
        Intent intent = new Intent(context,MapsActivity.class);
        intent.putExtra("latitude", holder.latitude);
        intent.putExtra("longitude", holder.longitude);
        intent.putExtra("locationName", holder.locationText.getText());
        context.startActivity(intent);
    }

    void setPlaces(List<Place> listPlaces){
        places = listPlaces;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (places != null)
            return places.size();
        else return 0;
    }

}
