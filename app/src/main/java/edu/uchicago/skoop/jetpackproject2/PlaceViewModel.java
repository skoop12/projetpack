package edu.uchicago.skoop.jetpackproject2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class PlaceViewModel extends AndroidViewModel {

    private PlaceRepository placeRepository;
    private LiveData<List<Place>> allPlaces;

    public PlaceViewModel (Application application) {
        super(application);
        placeRepository = new PlaceRepository(application);
        allPlaces = placeRepository.getAllPlaces();
    }

    LiveData<List<Place>> getAllPlaceData() {
        return allPlaces;
    }

    public void insertPlace(Place place) {
        placeRepository.insertPlace(place);
    }

}
